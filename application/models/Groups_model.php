<?php
class Groups_model extends CI_Model {

    public $name;
    public $team_id;
    public $date;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_last_ten_entries()
    {
        $query = $this->db->get('entries', 10);
        return $query->result();
    }

    public function insert_entry()
    {
        $this->name    = $_POST['name']; // please read the below note
        $this->team_id  = $_POST['team_id'];
        $this->date     = time();

        $this->db->insert('entries', $this);
    }

    public function update_entry()
    {
        $this->name    = $_POST['name'];
        $this->team_id  = $_POST['team_id'];
        $this->date     = time();

        $this->db->update('entries', $this, array('id' => $_POST['id']));
    }

    /**
     * Get all purchase info about each product registered on the system
     * such as Buyer, purchase datetime and etc.
     */
    public function getDatatablesList($limit = null, $offset = 0)
    {
        $orderable = [
            'name' => 'name',
        ];

        $query = $this->db
            ->select('SQL_CALC_FOUND_ROWS G.id, G.id, G.name, T.name as team, G.created_at', false)
            ->from('groups AS G')
            ->join('teams AS T', 'G.team_id = T.id', 'left')
        ;
        
        //Ao filtrar por "todos" no datatables, ele envia -1
        if ( $limit > 0 ) {
            $query
                ->limit($limit)
                ->offset($offset);
        }

        $this->datatablesQuery($query, [], $orderable);

        $result = $query->get()->result();
        $foundRows = $this->db->select('FOUND_ROWS() as found_rows')->get()->result_array()[0]['found_rows'];

        return ['foundRows' => $foundRows, 'data' => $result];
    }

}